﻿using System;
using Sy.stem.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BNumber
{
    struct BNumber
    {
        int[] a;
        int[] b;
        public int size;

        public BNumber(string s)
        {
            b = new int[1000];
            size = s.Length;
            a = new int[10000];
            for (int i=size-1, j=0;i>=0;i--,j++)
                a[j]=s[i]-'0';
        }

        public override string ToString()
        {
            string s = "";
            for (int i = size-1; i >=0; i--)
            {
                s =s + a[i].ToString();

            }
            return s;
        }

        public static BNumber operator +(BNumber x, BNumber y)
        {
            BNumber d = new BNumber("0");
            int k = 0;
            int l = Math.Max(x.size, y.size);
            d.size = l;
            for (int i = 0; i < l; i++)
            {
                d.a[i] = x.a[i] + y.a[i] + k;
                k = d.a[i] / 10;
                d.a[i] %= 10;
            }
            if (k > 0)
            {
                d.size++;
                d.a[d.size - 1] = k;
            }
            return d;
        }
        public static BNumber operator *(BNumber x, int y)
        {
            int h = 0;
            int v = 0;
            while (y != 0) 
            {
                int f = y % 10;
                x.b[v] = f;
                y=y/10;
                h=v;
                v++;
            }
            BNumber z = new BNumber("0");
            int l = Math.Max(x.size, h + 1);
            z.size = l;
            int k = 0;
            for (int i = 0,j=0; i < l; i++,j++)
            {
                z.a[i] = x.b[j]+x.a[i]+k;
                k = z.a[i] / 10;
                z.a[i] %= 10;
            }
            if (k > 0)
            {
                z.size++;
                z.a[z.size - 1] = k;
            }
            return z;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            string firstNumber = Console.ReadLine();
            string secondNumber = Console.ReadLine();
            int m = 154;
            BNumber a = new BNumber(firstNumber);
            BNumber b = new BNumber(secondNumber);
            BNumber d = a + b;
            BNumber g = a + a * m;
            Console.WriteLine(g);
            Console.ReadKey();
        }
    }
}
