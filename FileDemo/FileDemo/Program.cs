﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter writer = File.CreateText(@"d:\newfile.txt");
            writer.WriteLine("This is my new file");
            writer.WriteLine("Do you like its format?");
            writer.Close();
            StreamReader reader = File.OpenText(@"d:\newfile.txt");
            reader.Close();

            string path = "d:/newfile.txt";
            int i = 0;
            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    Console.WriteLine(sr.ReadLine());
                    i++;
                }
            }
            Console.WriteLine(i);
            Console.ReadKey();

        }
    }
}
