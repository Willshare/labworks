﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateStruct
{
    struct Person
    {
        public string firstName;
        public string lastName;
        public int age;
        public Genders gender;
        public Days day1;
        public Days day2;

        public Person(string _firstName, string _lastName, int _age, Genders _gender, Days _day1, Days _day2)
        {
            firstName = _firstName;
            lastName = _lastName;
            age = _age;
            gender = _gender;
            day1 = _day1;
            day2 = _day2;

        }

        public override string ToString()
        {
            return firstName + " " + lastName + "("+ gender+"),age" + age+" "+day1+day2;
        }

        public enum Genders : int { Male, Female };
        public enum Days : int { Pon, Vtr, Sre, Chet, Pyat, Sub, Voskr };



    }
    class Program
    {
        static void Main(string[] args)
        {
            Person P = new Person("Tony", "Allen", 32, Person.Genders.Male, Person.Days.Chet, Person.Days.Pon) ;
            Console.WriteLine(P);
            Console.ReadKey();
        }
    }
}
