﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ex2END
{
    public partial class Form1 : Form
    {
        MyButton[,] btns = new MyButton[8, 8];
        public Form1()
        {
            InitializeComponent();
        }

        public void btn_click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            int x = b.x;
            int y = b.y;
            if (x - 2 >= 0 && y - 2 >= 0 && x+2<=7 && y+2<=7)
            {
                if (btns[x - 1, y - 2].BackColor == Color.Black)
                    btns[x - 1, y - 2].BackColor = Color.White;
                else
                    btns[x - 1, y - 2].BackColor = Color.Black;
                if (btns[x - 2, y - 1].BackColor == Color.Black)
                    btns[x - 2, y - 1].BackColor = Color.White;
                else
                    btns[x - 2, y - 1].BackColor = Color.Black;
                if (btns[x - 2, y + 1].BackColor == Color.Black)
                    btns[x - 2, y + 1].BackColor = Color.White;
                else
                    btns[x - 2, y + 1].BackColor = Color.Black;
                if (btns[x - 1, y + 2].BackColor == Color.Black)
                    btns[x - 1, y + 2].BackColor = Color.White;
                else
                    btns[x - 1, y + 2].BackColor = Color.Black;
                if (btns[x + 1, y + 2].BackColor == Color.Black)
                    btns[x + 1, y + 2].BackColor = Color.White;
                else
                    btns[x + 1, y + 2].BackColor = Color.Black;
                if (btns[x + 2, y + 1].BackColor == Color.Black)
                    btns[x + 2, y + 1].BackColor = Color.White;
                else
                    btns[x + 2, y + 1].BackColor = Color.Black;
                if (btns[x + 2, y - 1].BackColor == Color.Black)
                    btns[x + 2, y - 1].BackColor = Color.White;
                else
                    btns[x + 2, y - 1].BackColor = Color.Black;
                if (btns[x + 1, y - 2].BackColor == Color.Black)
                    btns[x + 1, y - 2].BackColor = Color.White;
                else
                    btns[x + 1, y - 2].BackColor = Color.Black;
            }
            if (x ==0 && y - 2 >= 0 && y + 2 >= 0) 
            {
                if (btns[x + 1, y + 2].BackColor == Color.Black)
                    btns[x + 1, y + 2].BackColor = Color.White;
                else
                    btns[x + 1, y + 2].BackColor = Color.Black;
                if (btns[x + 2, y + 1].BackColor == Color.Black)
                    btns[x + 2, y + 1].BackColor = Color.White;
                else
                    btns[x + 2, y + 1].BackColor = Color.Black;
                if (btns[x + 2, y - 1].BackColor == Color.Black)
                    btns[x + 2, y - 1].BackColor = Color.White;
                else
                    btns[x + 2, y - 1].BackColor = Color.Black;
                if (btns[x + 1, y - 2].BackColor == Color.Black)
                    btns[x + 1, y - 2].BackColor = Color.White;
                else
                    btns[x + 1, y - 2].BackColor = Color.Black;
            }
            if (x == 7 && y - 2 >= 0 && y + 2 >= 0) 
            {
                if (btns[x - 1, y - 2].BackColor == Color.Black)
                    btns[x - 1, y - 2].BackColor = Color.White;
                else
                    btns[x - 1, y - 2].BackColor = Color.Black;
                if (btns[x - 2, y - 1].BackColor == Color.Black)
                    btns[x - 2, y - 1].BackColor = Color.White;
                else
                    btns[x - 2, y - 1].BackColor = Color.Black;
                if (btns[x - 2, y + 1].BackColor == Color.Black)
                    btns[x - 2, y + 1].BackColor = Color.White;
                else
                    btns[x - 2, y + 1].BackColor = Color.Black;
                if (btns[x - 1, y + 2].BackColor == Color.Black)
                    btns[x - 1, y + 2].BackColor = Color.White;
                else
                    btns[x - 1, y + 2].BackColor = Color.Black;
            }
            if (y == 0 && x - 2 >= 0 && x + 2 <= 7) 
            {
                if (btns[x - 2, y + 1].BackColor == Color.Black)
                    btns[x - 2, y + 1].BackColor = Color.White;
                else
                    btns[x - 2, y + 1].BackColor = Color.Black;
                if (btns[x - 1, y + 2].BackColor == Color.Black)
                    btns[x - 1, y + 2].BackColor = Color.White;
                else
                    btns[x - 1, y + 2].BackColor = Color.Black;
                if (btns[x + 1, y + 2].BackColor == Color.Black)
                    btns[x + 1, y + 2].BackColor = Color.White;
                else
                    btns[x + 1, y + 2].BackColor = Color.Black;
                if (btns[x + 2, y + 1].BackColor == Color.Black)
                    btns[x + 2, y + 1].BackColor = Color.White;
                else
                    btns[x + 2, y + 1].BackColor = Color.Black;
            }
            if (y == 7 & x - 2 >= 0 && x + 2 <= 7) 
            {
                if (btns[x - 1, y - 2].BackColor == Color.Black)
                    btns[x - 1, y - 2].BackColor = Color.White;
                else
                    btns[x - 1, y - 2].BackColor = Color.Black;
                if (btns[x - 2, y - 1].BackColor == Color.Black)
                    btns[x - 2, y - 1].BackColor = Color.White;
                else
                    btns[x - 2, y - 1].BackColor = Color.Black;
                if (btns[x + 2, y - 1].BackColor == Color.Black)
                    btns[x + 2, y - 1].BackColor = Color.White;
                else
                    btns[x + 2, y - 1].BackColor = Color.Black;
                if (btns[x + 1, y - 2].BackColor == Color.Black)
                    btns[x + 1, y - 2].BackColor = Color.White;
                else
                    btns[x + 1, y - 2].BackColor = Color.Black;
            }
            if (x == 6 && y - 2 >= 0 && y + 2 >= 0) 
            {
                if (btns[x - 1, y - 2].BackColor == Color.Black)
                    btns[x - 1, y - 2].BackColor = Color.White;
                else
                    btns[x - 1, y - 2].BackColor = Color.Black;
                if (btns[x - 2, y - 1].BackColor == Color.Black)
                    btns[x - 2, y - 1].BackColor = Color.White;
                else
                    btns[x - 2, y - 1].BackColor = Color.Black;
                if (btns[x - 2, y + 1].BackColor == Color.Black)
                    btns[x - 2, y + 1].BackColor = Color.White;
                else
                    btns[x - 2, y + 1].BackColor = Color.Black;
                if (btns[x - 1, y + 2].BackColor == Color.Black)
                    btns[x - 1, y + 2].BackColor = Color.White;
                else
                    btns[x - 1, y + 2].BackColor = Color.Black;
                if (btns[x + 1, y + 2].BackColor == Color.Black)
                    btns[x + 1, y + 2].BackColor = Color.White;
                else
                    btns[x + 1, y + 2].BackColor = Color.Black;
                if (btns[x + 1, y - 2].BackColor == Color.Black)
                    btns[x + 1, y - 2].BackColor = Color.White;
                else
                    btns[x + 1, y - 2].BackColor = Color.Black;
            }
            if (x == 1 && y - 2 >= 0 && y + 2 >= 0) 
            {
                if (btns[x - 1, y + 2].BackColor == Color.Black)
                    btns[x - 1, y + 2].BackColor = Color.White;
                else
                    btns[x - 1, y + 2].BackColor = Color.Black;
                if (btns[x + 1, y + 2].BackColor == Color.Black)
                    btns[x + 1, y + 2].BackColor = Color.White;
                else
                    btns[x + 1, y + 2].BackColor = Color.Black;
                if (btns[x + 2, y + 1].BackColor == Color.Black)
                    btns[x + 2, y + 1].BackColor = Color.White;
                else
                    btns[x + 2, y + 1].BackColor = Color.Black;
                if (btns[x + 2, y - 1].BackColor == Color.Black)
                    btns[x + 2, y - 1].BackColor = Color.White;
                else
                    btns[x + 2, y - 1].BackColor = Color.Black;
                if (btns[x + 1, y - 2].BackColor == Color.Black)
                    btns[x + 1, y - 2].BackColor = Color.White;
                else
                    btns[x + 1, y - 2].BackColor = Color.Black;
                if (btns[x - 1, y - 2].BackColor == Color.Black)
                    btns[x - 1, y - 2].BackColor = Color.White;
                else
                    btns[x - 1, y - 2].BackColor = Color.Black;

            }








            

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    btns[i, j] = new MyButton(i, j);
                    btns[i, j].Size = new Size(20, 20);
                    btns[i, j].Location = new Point(j * 25, i * 25);
                    btns[i, j].BackColor = Color.White;
                    btns[i, j].Click += btn_click;
                    this.Controls.Add(btns[i, j]);
                }
            }
        }
    }
}
