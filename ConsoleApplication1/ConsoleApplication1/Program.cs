﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Levenshtein{

    class LevenshteinDistance
    {
        public static int Compute(String s, String t)
        {
            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];
            if (n == 0)
                return m;
            if (m == 0)
                return n;
            for (int i = 0; i <= n; d[i, 0] = i++)
            {
            }
            for (int j = 0; j <= m;d[0,j]=j++ )
            {
            }
            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int z = (t[j - 1] == s[i - 1]) ? 0 : 2;
                    d[i, j] = Math.Min(Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1), d[i - 1, j - 1] + z);
                }
            }
            return d[n, m];
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader dic = new StreamReader(@"C:\lab_works\words.txt");
            StreamReader sr = new StreamReader(@"C:\lab_works\input.txt");
            StreamWriter sw = new StreamWriter(@"C:\lab_works\output.txt");
            string[] ar = sr.ReadToEnd().Split(' ');
            var words = new List<string>();
            string line;
            while ((line = dic.ReadLine()) != null)
            {
                words.Add(line);
            }
            for (int i = 0; i < ar.Length; i++)
            {
                string s = ar[i].ToLower();
                bool iscorrect = false;
                int mndist = -1;
                string tmp = " ";
                for (int j = 0; j < words.Count; j++)
                {
                    string t = words[j];
                    int dist = LevenshteinDistance.Compute(s, t);
                    if (dist == 0) iscorrect = true;
                    else if (mndist == -1 || mndist > dist)
                    {
                        mndist = dist;
                        tmp = t;
                    }
                }
                if (iscorrect == true)
                {
                    sw.Write(ar[i] + " ");
                    Console.WriteLine(ar[i] + " : ok!");
                }
                else
                {
                    sw.Write(tmp + " ");
                    Console.WriteLine(ar[i] + " -> " + tmp + "  " + mndist);
                }
            }
            Console.ReadKey();
            dic.Close();
            sr.Close();
            sw.Close();

        }
    }
}
