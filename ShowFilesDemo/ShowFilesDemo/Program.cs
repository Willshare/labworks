﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace ShowFilesDemo
{
    class Program
    {
        public static int i = 0;
        static void Main(string[] args)
        {
            DirectoryInfo dir = new DirectoryInfo("d:/asdd");
            ShowDirectory(dir);
            Console.ReadKey();

        }
        static void ShowDirectory(DirectoryInfo dir)
        {
            i++;
            Console.WriteLine(dir);
            foreach (FileInfo file in dir.GetFiles())
            {
                for (int j = 0; j < i ; j++)
                    Console.Write(" ");
                Console.WriteLine("File: {0}", file.FullName);
            }
            foreach (DirectoryInfo subDir in dir.GetDirectories())
            {
                for (int j=0;j<i;j++)
                    Console.Write(" ");
                ShowDirectory(subDir);
            }
            i--;
        }


    }
}
