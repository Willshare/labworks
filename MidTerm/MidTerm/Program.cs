﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace MidTerm
{
    class line 
    {
        public point _a;
        public point _b;
        public line(point a, point b) 
        {
            _a = a;
            _b = b;
        }
        public double lineLength()
        {
            double s;
            int x = _a._x - _b._x;
            int y = _a._y - _b._y;
            s=Math.Sqrt(Math.Pow(x,2) + Math.Pow(y,2));
            return s;
        }
    }
    class point
    {
        public int _x;
        public int _y;
        public point(int x, int y) 
        {
            
            _x = x;
            _y = y;
        }
        
        public override string ToString()
        {
            string s = "";
            s = _x.ToString() + "," + _y.ToString();
 	        return s;
        }
        public static point operator +(point x, point y)
        {
            point z = new point(0,0);
            z._x = x._x + y._x;
            z._y = x._y + y._y;
            return z;

        }
        
    }
    [Serializable] class polygon 
    {
        string _x;
        public polygon(string x)
        {
            _x = x;
        }
        public override string ToString()
        {
            return _x.ToString();
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            point a = new point(4,5);
            point b= new point(1,2);
            point z = a + b;
            line f = new line(a, b);
            List<point> myAL = new List<point>();
            using (StreamReader pol = new StreamReader("C:/Lab_works/polygon.txt"))
            {
                while (pol.Peek() >= 0)
                {
                    myAL.Add();
                    Console.WriteLine(pol.ReadLine());
                }
            }

            FileStream fs = new FileStream("C:/Lab_works/polygon1.txt", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, polygon);
            fs.Close();

            
            Console.WriteLine(f.lineLength());
            Console.ReadKey();
        }
    }
}
